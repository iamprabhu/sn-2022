
```javascript
const instructor = {
	name: "Prabhu Sunderaraman",
	email: "prabhu.bits@gmail.com",
	blog: "http://healthycoder.in",
	books: ["Spring 3.0 Black book","Practical Et JS 4"]
} 
```
<hr/>

## ToDo 

* __Not in specific order__

<br/>

*  Understanding services
*  Packaging and running services
*  Program Arguments


<br/>

*  Synchronous communication
*  Asynchronous communication
*  RestTemplate
*  Documenting Services using Swagger

<br/>

*	Spring boot JPA: Refresher
*	@Transaction
*   Distributed transactions
*	Spring data with MongoDB
*	Exercises

<br/>

*	JmsTemplate, JmsListener
*   ActiveMQ/RabbitMQ/Kafka _Depending on software availability_
*	Exercises


<br/>

*	Understanding Microservices architecture
*	Microservices ecosystem
*   Common Patterns
*   Challenges
*	Exercises


<br/>

*	Health check using Actuator
*	Securing actuator
*   Hypermedia
*	Unit testing Spring boot
*	Mocking
*	TestRestTemplate
*	Exercises


<br/>

*  Config Server
*  Spring Cloud Config
*  Profiles

<br/>

*	Eureka Discovery
*	API Gateway
*	Client-side Load Balancing
*	Circuit breaker using Resilience4J 
*	Exercises

<br/>

*	Security basics
*	Auto Configurations
*	Authentications
*	Introduction to securing credentials in Vault
*	Exercises


<hr/>
