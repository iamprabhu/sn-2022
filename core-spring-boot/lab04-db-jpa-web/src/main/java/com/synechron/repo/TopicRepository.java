package com.synechron.repo;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.synechron.entity.Topic;

public interface TopicRepository extends JpaRepository<Topic, Integer> {
	Optional<Topic> findByTitle(String title);
}
