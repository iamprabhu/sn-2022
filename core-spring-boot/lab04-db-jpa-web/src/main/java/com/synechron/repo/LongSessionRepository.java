package com.synechron.repo;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.synechron.entity.LongSession;

public interface LongSessionRepository extends JpaRepository<LongSession, Integer> {
	Optional<LongSession> findByTitle(String title);
}
