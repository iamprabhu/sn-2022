package com.synechron;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Lab04DbJpaWebApplication {

	public static void main(String[] args) {
		SpringApplication.run(Lab04DbJpaWebApplication.class, args);
	}

}
