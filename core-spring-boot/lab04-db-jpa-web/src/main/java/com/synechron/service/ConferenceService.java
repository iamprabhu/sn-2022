package com.synechron.service;

import java.util.List;
import java.util.Optional;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.synechron.entity.LongSession;
import com.synechron.entity.Topic;
import com.synechron.repo.LongSessionRepository;
import com.synechron.repo.TopicRepository;

@Service
public class ConferenceService {

	@Autowired
	private TopicRepository topicRepository;
	
	@Autowired
	private LongSessionRepository longSessionRepository;
	
	@Transactional
	public void addTopic(String title, int duration) {
		Topic topic = new Topic(title, duration);
		topicRepository.save(topic); //1
		if(duration > 60) {
			LongSession longSession = new LongSession(title);
			
			//Let's generate a silly error
			int[] arr = {10, 20};
			arr[4] = 100;
			
			longSessionRepository.save(longSession); //2
		}
	}
	
	public List<Topic> getAllTopics() {
		return topicRepository.findAll();
	} 
	
	public void removeTopic(String title) {
		Optional<Topic> optTopic = topicRepository.findByTitle(title);
		if(optTopic.isPresent()) {
			topicRepository.delete(optTopic.get());
		}
		Optional<LongSession> optLongSession = longSessionRepository.findByTitle(title);
		if(optLongSession.isPresent()) {
			longSessionRepository.delete(optLongSession.get());
		}
	}
}
