* Let's implement lab03 using JPA with a __small addition__

* Create a table __topics__ in __training__ database with the following fields
* __id__ int(primary key, auto increment), __title__(varchar) and __duration__ (number)


* Create another table __long_sessions__ in __training__ database with the following fields
* __id__ int(primary key, auto increment), __title__(varchar)


<br/>

* Create a new spring boot project __lab04-db-web__
* Add __Spring web, Spring data Jpa, MySQL driver, Dev tools__ dependencies
* You will implement the following in this application


<br/>

* Define __TopicsDao__ class which has methods to add/remove/list all topics
* Implement a __ConferenceService__ class that uses TopicDao

``` java 

public class ConferenceService {
	public boolean addTopic(String title, int duration) {
		//true if succesfully inserted
		//false if insertion fails
		
		//If the duration is greater than 60 minutes insert a record to long_sessions
	}
	
	public List<Topic> getAllTopics() {
	
	} 
	
	public boolean removeTopic(String title) {
	}
	
}

```

* Create two entity classes and two repository interfaces

* Create a RESTful controller __ConferenceController__ which uses __ConferenceService__
* Implement endpoints to insert/remove topic and get all topics. The design of the endpoints is upto your discretion. 
* Use Postman and insert topics, and display the topics
