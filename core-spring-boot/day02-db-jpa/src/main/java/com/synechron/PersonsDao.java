package com.synechron;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

//Container generates an implementation of this interface
//which will have all the methods to perform CRUD operations on Person class (persons table)
public interface PersonsDao extends JpaRepository<Person, Integer> {

	//findBy, findAllBy
	
	Person findByName(String name);
	Person findByNameAndAge(String name, int age);
	Person findByIdAndNameAndAge(int id, String name, int age);
	Person findByAgeAndName(int age, String name);
	
	
	List<Person> findAllByName(String name);
	List<Person> findAllByNameAndAge(String name, int age);
	List<Person> findAllByIdAndNameAndAge(int id, String name, int age);
	List<Person> findAllByAgeAndName(int age, String name);
	
	List<Person> findAllByAgeBetween(int start, int end);
	
	@Query(value = "select p from Person p where p.age > :p1") //JPA QL
	List<Person> findAllByAgeGt(@Param("p1") int age);
	
	@Query(value = "select name from persons where age >= :p1 and  age < :p2", nativeQuery = true)
	List<String> selectNamesOfPersonsWithAgeBetween(@Param("p1") int start, @Param("p2") int end);

	@Modifying
	@Query(value = "update persons set age = :p1 where id = :p2", nativeQuery = true)//Will work only within a transaction
	void updateAge(@Param("p2") int id, @Param("p1")int newAge);
}
