package com.synechron;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Day02DbJpaApplication implements CommandLineRunner {

	public static void main(String[] args) {
		SpringApplication.run(Day02DbJpaApplication.class, args);
	}

	@Autowired
	private PersonsDao personsDao;
	
	@Autowired
	private PersonService personService;
	
	@Override
	public void run(String... args) throws Exception {
		
		
		Person p1 = new Person("Mary", 30);
		
		Car c1 = new Car("BMW");
		c1.setOwner(p1);
		
		Car c2 = new Car("Ford");
		c2.setOwner(p1);
		
		List<Car> cars = new ArrayList<>();
		cars.add(c1);
		cars.add(c2);
	
		p1.setCars(cars);
		
		personsDao.save(p1);
		
		
		

//		personService.updateAge(4, 10);
//		//personsDao.updateAge(4, 10); //will not work
//		
//		List<Person> persons = personsDao.findAllByAgeBetween(10, 50);
//		System.out.println(persons);
		
//		Person person = personsDao.findByName("Sita");
//		System.out.println(person);
		
		
//		System.out.println(personsDao.getClass().getName());
//		System.out.println("*****Interfaces implemented********");
//		
//		for (Class cls : personsDao.getClass().getInterfaces()) {
//			System.out.println(cls.getName());
//		}
//		System.out.println("*****Methods*******");
//		for (Method method : personsDao.getClass().getMethods()) {
//			System.out.println(method.getName());
//		}
		
		
		//insert
//		Person p1 = new Person("Vikas", 44);
//		personsDao.save(p1); //generate an insert query; insert the record; get the id of the record; populate it in p1 object
//		System.out.println(p1.getId() + " is created");

//		//Read
//		List<Person> persons = personsDao.findAll();
//		System.out.println(persons);
//		
//		
//		//Delete a person with id 1
//		Optional<Person> optPerson = personsDao.findById(3);
//		if(optPerson.isPresent()) {
//			Person person = optPerson.get();
//			personsDao.delete(person);
//		}
//		
//		//Update the age of a person with 3
//		Optional<Person> optionalPerson = personsDao.findById(4);
//		int newAge = 110;
//		if(optionalPerson.isPresent()) {
//			Person person = optionalPerson.get();
//			person.setAge(newAge);
//			personsDao.save(person);
//		}
		
	}

}
