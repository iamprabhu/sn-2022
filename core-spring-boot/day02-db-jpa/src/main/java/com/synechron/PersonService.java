package com.synechron;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class PersonService {

	@Autowired
	private PersonsDao personsDao;
	
	@Transactional
	public void updateAge(int id, int newAge) {
		personsDao.updateAge(id, newAge);
	}
}
