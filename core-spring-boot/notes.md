* EJB (Enterprise Java Beans)

``` java
class Account {
	accountNumber, balance;
	void deposit(amount) {
		//talk to DB
	}
	void withdraw(amount) {
		//talk to DB
	}
	void transferFund(to, amount) {
		//talk to several DBs
	}
}

class Customer {
}
```

* Object pooling for Account class
* Transaction functionality for __transferFund__ method
* Security service for __withdraw__ and __transferFund__ methods

* EJB
* Download me; Install me; Configure me; Start me;Drop your code here; I will provide the services for you
* EJB is a __heavyweight__ container

``` java
interface AccountHome extends Home {}
interface Account extends Remote {
	deposit(), withdraw(), transferFund()
}
class AccountBean implements EntityBean {
	//Implement the methods
	//Don't write constructors
	//Don't have static methods
	//Don't use throws keyword in your method
	//Don't have final variables
}
//ejb-jar.xml
//Zip all these contents
//Drop them into the container
//Configure the container and specify that AccountBean needs transaction, object pooling and security
```

### Enter spring

* I will provide all the enterprise services for the beans you write
* You don't need to change the design of the code
* Spring framework is known as a __lightweight__ container

* POJO framework
* __Dependency Injection__: Simple mechanism by which you avoid writing unneccessary creation and lookup code
* Console, Web application, Producer/Consumer, Jobs
* 5.x is the latest version
* Getting started and working in a spring project required a lot of effort
* You have add several dependencies; configure a lot of items; 
* Cloud made moving applications into production as agile as possible
* Need a way by which you can very quickly get started working with your applications and deploy them into production asap
* __Spring boot__ comes into picture
* Spring boot is an abstraction on Spring
* Latest version is 2.x
* You open your IDE; Select what kind of application you want to build; All the configurations will be done for you and given to you


### Spring configuration

* Mark the classes as __@Component__
* Use __@Autowired__ to wire the beans together
* All the beans are in singleton scope by default. You can change the scope by using __@Scope__


### Configuring collections

* You can configure a collection in properties file
```
topics:
 topicsWithDuration60Min: A, B, C
```

* You can inject __topics.topicsWithDuration60Min__ into a list variable like this

``` java
@Component
class SessionPlanner {
	@Value("${topics.topicsWithDuration60Min}")
	private List<String> topics;
}

```

* You can configure a collection in YML file like this
```
topics:
 topicsWithDuration60Min: 
  - A
  - B
  - C
```

* Use the __topics__ as the prefix and have a variable that matches the same name as the collection name

``` java
@Component
@ConfigurationProperties(prefix = "topics")
class SessionPlanner {
	private List<String> topicsWithDuration60Min;
		
	public void setTopicsWithDuration60Min(List<String> topicsWithDuration60Min) {
		this.topicsWithDuration60Min = topicsWithDuration60Min;
	}
}
```




### ComponentScan

```
  [day01-common-lib]
  	[com.mycompany]
		- *.java

  [day01]
  	[com.synechron]
		- *.java
		- Day01Application.java
```

* In the main class of __day01__ you list the day01-common-lib packages[__com.mycompany__] in the __@ComponentScan__ annotation in Day01Application

### Day 01

* @Component/@Service, @Autowired, @Value, @Qualifier, @Bean, @Configuration, @ComponentScan, @Scope, @Lazy
* ApplicationContext is the active container
* @Controller/@RestController
* @RestController returns data
* @Controller returns a view that needs to be resolved to a physical page/file
* @GetMapping, @PostMapping, @PutMapping, @DeleteMapping, @PatchMapping
* Heart of spring boot web application is __DispatcherServlet__
* All the components are in singleton scope by default



### Transactions

* Consistent outcome
* Either complete success or complete failure
* Managing transactions in applications involves drawing the boundaries
* You specify when to start, when to end, when to commit and when to rollback
* A set of operations like DB updates, Sending an email, Posting to a queue can be part of a transaction;
* Usually only database operations can be rolled back
* Programtic and Declarative ways of transaction management
* __TransactionTemplate__ in Spring is used for programmatic management
* __@Transactional__ is the annotation used to declaratively manage transactions






























