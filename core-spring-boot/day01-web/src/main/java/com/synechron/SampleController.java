package com.synechron;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class SampleController {

	//http://localhost:9090/greet -> Hello there!
	
	@GetMapping("/greet")
	public String greet() {
		return "Hello there!";
	}
	
	//http://localhost:9090/hi/Ram -> Hi Ram!
	//http://localhost:9090/hi/Sam -> Hi Sam!
	
	@GetMapping("/hi/{name}")
	public String greetWithHi(@PathVariable("name") String name) {
		return "Hi " + name + "!";
	}
	
	//http://localhost:9090/bye?name=Ram -> Bye Ram!
	//http://localhost:9090/bye?name=Thara -> Bye Thara!
	
	@GetMapping("/bye")
	public String greetWithBye(@RequestParam("name") String name) {
		return "Bye " + name + "!";
	}
	
	@PostMapping("/greetings")
	public String greetings() {
		return "Hello there!";
	}
	
	//http://localhost:9090/hi/Ram -> Hi Ram!
	//http://localhost:9090/hi/Sam -> Hi Sam!
	
	@PostMapping("/hello/{name}")
	public String greetingsWithHi(@PathVariable("name") String name) {
		return "Hi " + name + "!";
	}
	
	//http://localhost:9090/bye?name=Ram -> Bye Ram!
	//http://localhost:9090/bye?name=Thara -> Bye Thara!
	
	@PostMapping("/byebye")
	public String greetingsWithBye(@RequestParam("name") String name) {
		return "Bye " + name + "!";
	}
}
