package com.synechron;

import java.time.Instant;
import java.time.LocalTime;

import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

//@RestController = @Controller + @ResponseBody
@Controller
public class PlainController {

	@GetMapping("/now")
	@ResponseBody
	public String getCurrentDatetime() {
		return Instant.now().toString(); // Will not be resolved to a view; The data will be directly written to the response
	}
	
	@PostMapping("/processcomments")
	public String processComments(@RequestParam String comments, HttpSession session) {
		String message = "Thanks for your valuable feedback.<br/>";
		message += " Your comments '" + comments + "' is submitted";
		session.setAttribute("message", message);
		return "result";
	}
	
	@GetMapping("/hello")
	public String greet() {
		return "hello"; //Hello here does not indicate plain data; It has to resolved to a view
	}
}
