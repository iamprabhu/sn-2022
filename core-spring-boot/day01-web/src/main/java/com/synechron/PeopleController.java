package com.synechron;

import java.util.Arrays;
import java.util.List;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class PeopleController {

	
	//public String addPerson(String name, int age, String gender) {}
	
	@PostMapping("/person")
	public String addPerson(@RequestBody Person person) {
		System.out.println(person.getName() + ", " + person.getAge() + ", " + person.getGender());
		return "Added";
	}
	
	@GetMapping("/people") 
	public List<Person> getPersons() {
		return Arrays.asList(
				new Person("Sam", 10, "Male"),
				new Person("Kim", 30, "Female"),
				new Person("Virat", 40, "Male")
		);
	}
	
	@GetMapping("/names")
	public List<String> getPeopleNames() {
		return Arrays.asList("Sam", "Ram", "Virat", "Tara");
	}
}
