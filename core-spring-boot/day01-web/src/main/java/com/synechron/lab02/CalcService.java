package com.synechron.lab02;

import org.springframework.stereotype.Service;

@Service
public class CalcService {
	
	public int add(int num1, int num2) {
		return num1 + num2;
	}
	
	public int subtract(int num1, int num2) {
		return num1 - num2;
	}
	
	public int multiply(int num1, int num2) {
		return num1 * num2;
	}
	
	public int square(int num) {
		return num * num;
	}
}
