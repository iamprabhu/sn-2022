package com.synechron.lab02;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/calc")
public class CalcController {

	@Autowired
	private CalcService calcService;
	
	@GetMapping("/add/{num1}/{num2}")
	public int add(@PathVariable int num1, @PathVariable int num2) {
		return calcService.add(num1, num2);
	}
	
	@GetMapping("/square/{num}")
	public int square(@PathVariable int num) {
		return calcService.square(num);
	}
	
	@PutMapping("/subtract/{num1}/{num2}")
	public int subtract(@PathVariable int num1, @PathVariable int num2) {
		return calcService.subtract(num1, num2);
	}
	
	@PostMapping("/product")
	public int multiply(@RequestParam int num1, @RequestParam int num2) {
		return calcService.multiply(num1, num2);
	}
}
