package com.synechron;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

@Component
public class PersonsDao {

	@Autowired
	private JdbcTemplate jdbcTemplate;
	
	
	public void deletePerson(int id) {
		String query = "delet from persons where id = ?"; //DELIBERATE ERROR IN QUERY
		jdbcTemplate.update(query, id);
	}
	
	public void savePerson(String name, int age) {
		String query = "insert into persons(name, age) values(?, ?)";
		jdbcTemplate.update(query, name, age);
	}
	
	public Object selectAllPersons() {
		String query = "select * from persons";
		List<Map<String, Object>> records = jdbcTemplate.queryForList(query);
		return records;
	}
	
	public List<Person> getAllPersons() {
		String query = "select * from persons";
		//DO NOT TRY TO REMEMBER THIS; All we are doing is, using a built-in method convert a row to Person object
		List<Person> persons = jdbcTemplate
				.query(
						query, 
						(ResultSet rs, int rowNum) -> new Person(rs.getInt("id"), rs.getString("name"), rs.getInt("age")));
		return persons;
	}
	
	
}
