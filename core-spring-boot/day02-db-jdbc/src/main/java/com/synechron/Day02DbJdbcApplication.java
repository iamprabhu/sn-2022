package com.synechron;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.dao.DataAccessException;

@SpringBootApplication
public class Day02DbJdbcApplication implements CommandLineRunner {

	public static void main(String[] args) {
		SpringApplication.run(Day02DbJdbcApplication.class, args);
	}

	@Autowired
	private PersonsDao personsDao;
	
	@Override
	public void run(String... args) throws Exception {
		//personsDao.savePerson("Sam", 20);
		//System.out.println(personsDao.selectAllPersons());
		//System.out.println(personsDao.getAllPersons());
		
		try {
			personsDao.deletePerson(101);
		} catch (DataAccessException e) {
			System.out.println(e.getCause().getMessage());
		}
	}
	
	

}
