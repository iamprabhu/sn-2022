package com.synechron;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

public class PersonsDaoUsingRawJdbc {

	public void savePersons(String name, int age) throws SQLException {
		Connection con = null;
		try {
			Class.forName("com.mysql.jdbc.Driver");
			con = DriverManager.getConnection("jdbc:mysql://localhost/training", "username", "password");
			Statement stmt = con.createStatement();
			String query = "insert into persons(name, age) values('" + name + "', " + age + ")";
			stmt.execute(query);
			stmt.close();
			
		} catch (ClassNotFoundException | SQLException e) {
			e.printStackTrace();
		}
		finally {
			con.close();
		}
	}
}
