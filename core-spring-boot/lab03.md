
* Create a table __topics__ in __training__ database with the following fields
* __id__ int(primary key, auto increment), __title__(varchar) and __duration__ (number)

<br/>

* Create a new spring boot project __lab03-db-web__
* Add __Spring web, Spring data Jdbc, MySQL driver, Dev tools__ dependencies
* You will implement the following in this application


<br/>

* Define __TopicsDao__ class which has methods to add/remove/list all topics
* Implement a __ConferenceService__ class that uses TopicDao

``` java 

public class ConferenceService {
	public boolean addTopic(String title, int duration) {
		//true if succesfully inserted
		//false if insertion fails
	}
	
	public List<Topic> getAllTopics() {
	
	} 
	
	public boolean removeTopic(String title) {
	}
	
}

```

* Create a RESTful controller __ConferenceController__ which uses __ConferenceService__
* Implement endpoints to insert/remove topic and get all topics. The design of the endpoints is upto your discretion. 
* Use Postman and insert topics, and display the topics
