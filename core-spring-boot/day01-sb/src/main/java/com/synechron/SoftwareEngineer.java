package com.synechron;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

@Component
public class SoftwareEngineer {

	@Autowired
	//@Qualifier("fineArts")
	private List<String> hobbies;

	public List<String> getHobbies() {
		return hobbies;
	}
	
}
