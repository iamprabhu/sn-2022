package com.synechron;

import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Component;

@Component
@Lazy
public class Book {
	public Book() {
		System.out.println("-----Book created");
	}

}
