package com.synechron;

import java.util.Arrays;
import java.util.List;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;

//This is like creating a configuration file
@Configuration
public class Day01Configuration {

	@Bean
	public List<String> fineArts() {
		return Arrays.asList("Dance", "Carnatic music", "Literature");
	}
	
	@Bean
	@Primary
	public List<String> swHobbies() {
		return Arrays.asList("Netflix", "Prime", "Zerodha");
	}
}
