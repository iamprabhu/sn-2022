package com.synechron;

import org.springframework.stereotype.Component;

@Component
public class HelloWorld {

	public HelloWorld() {
		System.out.println("**HelloWorld object created");
	}
	public String greet(String name) {
		return "Hello " + name;
	}
}
