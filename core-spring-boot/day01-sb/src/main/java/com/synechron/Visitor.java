package com.synechron;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class Visitor {

	@Autowired
	private Company companyToVisit;

	public Company getCompanyToVisit() {
		return companyToVisit;
	}
	
}
