package com.synechron;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class Person {
	
	public Person(@Value("Ram") String name) {
		this.name = name;
		System.out.println("Person created");
	}

	@Value("Ram")
	private String name;

	public String getName() {
		return name;
	}
	
}
