package com.synechron;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class Employee {
	@Autowired
	private Company workPlace;

	public Company getWorkPlace() {
		return workPlace;
	}
	
}
