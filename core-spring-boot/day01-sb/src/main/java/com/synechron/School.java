package com.synechron;

import java.util.List;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Component
@ConfigurationProperties(prefix = "school")
public class School {

	@Value("${school.schoolname}")
	private String name;
	
	@Value("${students.count:1000}")
	private long numberOfStudents;
	
//	@Value("${school.locations}")
//	private List<String> locations;

	private List<String> locations;
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}

	public long getNumberOfStudents() {
		return numberOfStudents;
	}

	public List<String> getLocations() {
		return locations;
	}

	public void setLocations(List<String> locations) {
		this.locations = locations;
	}
	
	
}
