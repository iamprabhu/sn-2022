package com.synechron;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

import com.lab01.Conference;

//In a console application, you want a method as a starting point
//This method should be called after all the initialization work is done


@SpringBootApplication
@ComponentScan(basePackages = "com")
public class Day01SbApplication implements CommandLineRunner {

	public static void main(String[] args) {
		SpringApplication.run(Day01SbApplication.class, args);
	}

	@Autowired
	private HelloWorld helloWorld;
	
	@Autowired
	private Car car;
	
	@Autowired
	private Employee employee;
	
	@Autowired
	private Visitor visitor;
	
	@Autowired
	private Person person;

	@Autowired
	private Book book;
	
	@Autowired
	private SoftwareEngineer softwareEngineer;
	
	@Autowired
	private School school;
	
	@Autowired
	private Conference conference;
	
	@Override
	public void run(String... args) throws Exception {
		System.out.println("All set to start learning spring boot");
		
		
		System.out.println("60 mins topics count: " + conference.get60MinsTopicsCount());
		System.out.println("50 mins topics count: " + conference.get50MinsTopicsCount());
		System.out.println("45 mins topics count: " + conference.get45MinsTopicsCount());
		
		
//		System.out.println(school.getName());
//		System.out.println(school.getNumberOfStudents());
//		System.out.println(school.getLocations());
		
		
		//System.out.println(softwareEngineer.getHobbies());
		
		
		//System.out.println(person.getName());
		
		
//		System.out.println(employee.getWorkPlace());
//		System.out.println(visitor.getCompanyToVisit());
		
		
		
		
		
		
		
		
		//Don't write code to create an object; I have a container that will do the job
		//HelloWorld helloWorld = new HelloWorld();
//		
//		String message = helloWorld.greet("Sam");
//		System.out.println(message);
//		System.out.println(car);
		
	}

	
}
