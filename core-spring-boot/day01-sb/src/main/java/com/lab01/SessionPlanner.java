package com.lab01;

import java.util.List;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Component
@ConfigurationProperties(prefix = "topics")
public class SessionPlanner {
	
	private List<String> topicsWith60MinDuration;
	private List<String> topicsWith50MinDuration;
	private List<String> topicsWith45MinDuration;
	
	public List<String> getTopicsWith60MinDuration() {
		return topicsWith60MinDuration;
	}
	public void setTopicsWith60MinDuration(List<String> topicsWith60MinDuration) {
		this.topicsWith60MinDuration = topicsWith60MinDuration;
	}
	public List<String> getTopicsWith50MinDuration() {
		return topicsWith50MinDuration;
	}
	public void setTopicsWith50MinDuration(List<String> topicsWith50MinDuration) {
		this.topicsWith50MinDuration = topicsWith50MinDuration;
	}
	public List<String> getTopicsWith45MinDuration() {
		return topicsWith45MinDuration;
	}
	public void setTopicsWith45MinDuration(List<String> topicsWith45MinDuration) {
		this.topicsWith45MinDuration = topicsWith45MinDuration;
	}
	
	
	
	
}
