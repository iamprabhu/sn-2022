package com.lab01;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class Conference {
	
	@Autowired
	private SessionPlanner sessionPlanner;

	public int get60MinsTopicsCount() {
		return sessionPlanner.getTopicsWith60MinDuration().size();
	}
	
	public int get50MinsTopicsCount() {
		return sessionPlanner.getTopicsWith50MinDuration().size();
	}
	
	public int get45MinsTopicsCount() {
		return sessionPlanner.getTopicsWith45MinDuration().size();
	}
}
