package com.synechron.web;

import java.time.Instant;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.dao.DataAccessException;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.context.request.WebRequest;

import com.synechron.exception.TopicInsertionException;
import com.synechron.exception.TopicRemovalException;

@RestControllerAdvice //Use @ControllerAdvice for exception handling in @Controller methods
@Component
public class AppExceptionHandler {
	
	Logger logger = LoggerFactory.getLogger(AppExceptionHandler.class);
	
	@ExceptionHandler(TopicRemovalException.class)
	public HttpEntity<ErrorInformation> handleTopicRemovalException(TopicRemovalException ex, WebRequest req) {
		String message = "Error removing topic: " + ex.getMessage() + ". ";
		message += "The title may not be found in our database";
		ErrorInformation errorInformation = new ErrorInformation();
		errorInformation.setMessage(message);
		errorInformation.setTimestamp(Instant.now().toString());
		errorInformation.setPath(req.getDescription(false));
		logger.error("Error while removing topic {}", message);
		
		return new ResponseEntity<>(errorInformation, HttpStatus.NOT_FOUND);
	}
	
	@ExceptionHandler(TopicInsertionException.class)
	public HttpEntity<ErrorInformation> handleTopicInsertionException(TopicInsertionException ex, WebRequest req) {
		String message = "Error creating topic: " + ex.getMessage() + ". ";
		ErrorInformation errorInformation = new ErrorInformation();
		errorInformation.setMessage(message);
		errorInformation.setTimestamp(Instant.now().toString());
		errorInformation.setPath(req.getDescription(false));
		logger.error("Error while creating topic {}", message);
		
		return new ResponseEntity<>(errorInformation, HttpStatus.INTERNAL_SERVER_ERROR);
	}
	
	@ExceptionHandler(DataAccessException.class)
	public HttpEntity<ErrorInformation> handleDataAccessException(DataAccessException ex, WebRequest req) {
		String message = "Error: " + ex.getCause().getMessage();
		ErrorInformation errorInformation = new ErrorInformation();
		errorInformation.setMessage(message);
		errorInformation.setTimestamp(Instant.now().toString());
		errorInformation.setPath(req.getDescription(false));
		logger.error("Error during DB operations in topic reason: {} path: {}", message, req.getDescription(false));
		
		return new ResponseEntity<>(errorInformation, HttpStatus.INTERNAL_SERVER_ERROR);
	}
	
}
