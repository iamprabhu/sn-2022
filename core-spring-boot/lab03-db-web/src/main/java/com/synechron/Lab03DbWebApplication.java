package com.synechron;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Lab03DbWebApplication {

	public static void main(String[] args) {
		SpringApplication.run(Lab03DbWebApplication.class, args);
	}

}
