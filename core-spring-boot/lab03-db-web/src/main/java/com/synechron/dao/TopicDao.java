package com.synechron.dao;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import com.synechron.model.Topic;

@Repository
public class TopicDao {
	
	@Autowired
	private JdbcTemplate jdbcTemplate;
	
	public boolean add(String title, int duration) {
		String query = "insert into topics(title, duration) values(?, ?)";
		int numberOfRowsAffected = jdbcTemplate.update(query, title, duration);
		return numberOfRowsAffected == 1 ? true : false;
	}
	
	public boolean delete(String title) {
		String query = "delete from topics where title = ?";
		int numberOfRowsDeleted = jdbcTemplate.update(query, title);
		return numberOfRowsDeleted > 0 ? true : false;
	}
	
	public List<Topic> getAll() {
		String query = "select * from topics";
		return jdbcTemplate.query(query, (rs, num) -> new Topic(rs.getInt("id"), rs.getString("title"), rs.getInt("duration")));
	}
}
