package com.synechron.controller;

import java.time.Instant;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.request.WebRequest;

import com.synechron.exception.TopicRemovalException;
import com.synechron.model.Topic;
import com.synechron.service.ConferenceService;
import com.synechron.web.ErrorInformation;

@RestController
@RequestMapping("/conference/topic")
public class ConferenceController {

	@Autowired
	private ConferenceService conferenceService;
	
	//In URLs verbs are not allowed; /insert /get /fetch /remove /delete
	//In URLs nouns are used 
	
	@PostMapping("/title/{title}/duration/{duration}")
	public HttpEntity<Void> addTopic(@PathVariable String title, @PathVariable int duration) {
		conferenceService.addTopic(title, duration);
		return new ResponseEntity<>(HttpStatus.OK);
	}
	
	@DeleteMapping("/title/{title}")
	public HttpEntity<Void> removeTopic(@PathVariable String title) {
		conferenceService.removeTopic(title);	
		return new ResponseEntity<>(HttpStatus.OK);
	}
	
	@GetMapping("/all")
	public HttpEntity<List<Topic>> getTopics() {
		List<Topic> topics = conferenceService.getAllTopics();
		return new ResponseEntity<List<Topic>>(topics, HttpStatus.OK);
	}
	
	@PutMapping("/title/{title}/duration/{duration}")
	public void updateDuration(String title, int duration) {
		
	}
	
}
