package com.synechron.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.synechron.dao.TopicDao;
import com.synechron.exception.TopicInsertionException;
import com.synechron.exception.TopicRemovalException;
import com.synechron.model.Topic;

@Service
public class ConferenceService {

	@Autowired
	private TopicDao topicDao;
	
	public void addTopic(String title, int duration) {
		boolean topicAdded = topicDao.add(title, duration);
		if(!topicAdded) {
			throw new TopicInsertionException(title);
		}
	}
	
	public List<Topic> getAllTopics() {
		return topicDao.getAll();
	} 
	
	public void removeTopic(String title) {
		boolean topicRemoved = topicDao.delete(title);
		if(!topicRemoved) {
			throw new TopicRemovalException(title);
		}
	}

}
