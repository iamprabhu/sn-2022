package com.synechron.exception;

public class TopicInsertionException extends RuntimeException {
	public TopicInsertionException(String title) {
		super(title);
	}
}
