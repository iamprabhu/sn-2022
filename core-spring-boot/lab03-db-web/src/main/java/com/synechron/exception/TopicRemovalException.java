package com.synechron.exception;

public class TopicRemovalException extends RuntimeException {
	public TopicRemovalException(String title) {
		super(title);
	}
}
