package com.synechron;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class WarmupController {

	@Value("${server.port}")
	private int port;
	
	@GetMapping("/welcome")
	public String getWelcomeMessage() {
		return "Welcome to the training. Port is " + port;
	}
}
