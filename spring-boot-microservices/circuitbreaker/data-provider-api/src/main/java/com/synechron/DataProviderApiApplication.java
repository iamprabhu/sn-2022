package com.synechron;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DataProviderApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(DataProviderApiApplication.class, args);
	}

}
