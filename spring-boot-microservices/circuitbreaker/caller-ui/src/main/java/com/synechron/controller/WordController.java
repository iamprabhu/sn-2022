package com.synechron.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.client.circuitbreaker.CircuitBreakerFactory;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.synechron.service.WordService;

import io.github.resilience4j.circuitbreaker.annotation.CircuitBreaker;

@RestController
public class WordController {

	@Autowired
	private WordService wordService;
	
	@Autowired
	private CircuitBreakerFactory cbFactory;
	
	
	//@CircuitBreaker(name = "palindromeCircuitBreaker", fallbackMethod = "checkPalindromeTemp")
	@PostMapping("/wordplay")
	public HttpEntity<String> checkPalindrome(@RequestParam String word) {
		return cbFactory
			.create("palindromeCircuitBreaker")
			.run(
					() -> {
						System.out.println("***** original logic called");
						boolean result = wordService.isPalindrome(word);
						String message = word + " is not a palindrome";
						if(result) {
							message = word + " is a palindrome";
						}
						return new ResponseEntity<>(message, HttpStatus.OK);
					}, 
					(t) -> {
						System.out.println("-----Fallback called");
						//Post a message to a MoM
						return checkPalindromeTemp(word);
					}
			);
		
	}
	
	private HttpEntity<String> checkPalindromeTemp(String word) {
		String message = "Oops! Server is down. Unable to process " + word + ". Please try later";
		return new ResponseEntity<>(message, HttpStatus.OK);
	}
}
