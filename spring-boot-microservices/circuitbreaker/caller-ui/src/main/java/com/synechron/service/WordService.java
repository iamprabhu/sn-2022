package com.synechron.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
public class WordService {
	
	@Autowired
	private RestTemplate restTemplate;
	
	
	@Value("${palindrome.service.base.url}")
	private String palindromeBaseUrl;
	
	public boolean isPalindrome(String word) {
		String url = palindromeBaseUrl + "/" + word;
		boolean response = restTemplate.getForObject(url, Boolean.class);
		return response;
	}

}
