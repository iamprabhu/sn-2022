package com.synechron;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CallerUiApplication {

	public static void main(String[] args) {
		SpringApplication.run(CallerUiApplication.class, args);
	}

}
