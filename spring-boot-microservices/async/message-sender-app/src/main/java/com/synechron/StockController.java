package com.synechron;

import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class StockController {

	@Autowired
	private RabbitTemplate rabbitTemplate;
	
	@GetMapping("/stock/{symbol}")
	public String getPrice(@PathVariable String symbol) {
		rabbitTemplate.convertAndSend("StockQueue", symbol);
		return "You'll receive the stock value soon";
	}

}
