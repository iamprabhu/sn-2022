package com.synechron;

import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

@Component
public class StockReceiver {

	
	@RabbitListener(queues = "StockQueue")
	public void receiveStock(String symbol) {
		System.out.println("***Received " + symbol + ". CMP: " + Math.random() * 1000);
	}
}
