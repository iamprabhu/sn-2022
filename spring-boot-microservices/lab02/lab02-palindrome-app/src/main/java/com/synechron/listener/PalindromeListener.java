package com.synechron.listener;

import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.synechron.service.PalindromeService;

@Component
public class PalindromeListener {

	@Autowired
	private PalindromeService palindromeService;
	
	@Autowired
	private RabbitTemplate rabbitTemplate;
	
	@Value("${queue.palindrome}")
	private String palindromeQueue;
	
	@RabbitListener(queues = "${queue.word}")
	public void receiveWord(String word) {
		boolean result = palindromeService.isPalindrome(word);
		String csvMessage = word + "," + result;
		rabbitTemplate.convertAndSend(palindromeQueue, csvMessage);
	}
}
