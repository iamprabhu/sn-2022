package com.synechron.service;

import org.springframework.stereotype.Service;

@Service
public class PalindromeService {

	public boolean isPalindrome(String word) {
		StringBuilder stringBuilder = new StringBuilder(word);
		return stringBuilder.reverse().toString().equalsIgnoreCase(word);
	}
}
