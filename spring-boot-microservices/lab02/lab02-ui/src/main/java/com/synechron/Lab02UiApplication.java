package com.synechron;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Lab02UiApplication {

	public static void main(String[] args) {
		SpringApplication.run(Lab02UiApplication.class, args);
	}

}
