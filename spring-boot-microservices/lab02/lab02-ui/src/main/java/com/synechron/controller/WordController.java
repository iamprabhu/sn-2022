package com.synechron.controller;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.synechron.service.WordService;

@RestController
public class WordController {

	@Autowired
	private WordService wordService;
	
	@Autowired
	private Map<String, Boolean> palindromeResultsMap;
	
	@PostMapping("/wordplay")
	public HttpEntity<String> checkPalindrome(@RequestParam String word) {
		wordService.processWord(word);
		return new ResponseEntity<>("You'll receive the result soon", HttpStatus.OK);
	}
	
	@GetMapping("/results") 
	public Map<String, Boolean> getResultsMap() {
		return palindromeResultsMap;
	}
	
	@GetMapping("/result/{word}") 
	public HttpEntity<Boolean> getResultOf(@PathVariable String word) {
		return new ResponseEntity<>(palindromeResultsMap.get(word), HttpStatus.OK);
	}
}
