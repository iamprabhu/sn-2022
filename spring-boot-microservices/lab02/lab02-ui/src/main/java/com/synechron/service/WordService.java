package com.synechron.service;

import java.util.Map;

import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
public class WordService {
	
	@Autowired
	private RabbitTemplate rabbitTemplate;
	
	@Value("${queue.word}")
	private String wordQueue;
	
	@Autowired
	private Map<String, Boolean> palindromeResultsMap;
	
	public void processWord(String word) {
		rabbitTemplate.convertAndSend(wordQueue, word);
	}
	
	@RabbitListener(queues = "${queue.palindrome}")
	public void receivePalindromeResult(String resultInCsv) {
		String word = resultInCsv.split(",")[0];
		boolean result = Boolean.parseBoolean(resultInCsv.split(",")[1]);
		palindromeResultsMap.put(word, result);
	}
	

}
