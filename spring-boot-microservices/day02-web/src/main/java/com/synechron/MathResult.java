package com.synechron;

import org.springframework.hateoas.RepresentationModel;

public class MathResult extends RepresentationModel {
	private String operation;
	private int result;
	
	public String getOperation() {
		return operation;
	}
	public void setOperation(String operation) {
		this.operation = operation;
	}
	public int getResult() {
		return result;
	}
	public void setResult(int result) {
		this.result = result;
	}
	
}
