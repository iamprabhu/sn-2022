package com.synechron;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

import org.springframework.hateoas.RepresentationModel;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class AppController {

	@GetMapping("/math/{a}/{b}")
	public HttpEntity<RepresentationModel> getOperations(@PathVariable int a, @PathVariable int b) {
		RepresentationModel model = new RepresentationModel<>();
		model.add(linkTo(methodOn(CalcController.class).sum(a, b)).withRel("Addition"));
		model.add(linkTo(methodOn(CalcController.class).subtract(a, b)).withRel("Subtraction"));
		model.add(linkTo(methodOn(CalcController.class).multiply(a, b)).withRel("Multiplication"));
		model.add(linkTo(methodOn(CalcController.class).divide(a, b)).withRel("Division"));
		model.add(linkTo(methodOn(AlgebraController.class).aPlusB(a, b)).withRel("(a+b)^2"));
		return new ResponseEntity<RepresentationModel>(model, HttpStatus.OK);
	}
}
