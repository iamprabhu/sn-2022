package com.synechron;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class AlgebraController {

	@GetMapping("/aPlusbWholeSquare/{a}/{b}")
	public HttpEntity<Integer> aPlusB(@PathVariable int a, @PathVariable int b) {
		return new ResponseEntity<>(a*a + b*b + 2*a*b, HttpStatus.OK);
	}
	
}
