package com.synechron;

import org.springframework.hateoas.RepresentationModel;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.*;

@RestController
@RequestMapping("/calc")
public class CalcController {

	
	@GetMapping("/operations/{a}/{b}")
	public HttpEntity<RepresentationModel> getOperations(@PathVariable int a, @PathVariable int b) {
		RepresentationModel model = new RepresentationModel<>();
		model.add(linkTo(methodOn(this.getClass()).sum(a, b)).withRel("Addition"));
		model.add(linkTo(methodOn(this.getClass()).subtract(a, b)).withRel("Subtraction"));
		model.add(linkTo(methodOn(this.getClass()).multiply(a, b)).withRel("Multiplication"));
		model.add(linkTo(methodOn(this.getClass()).divide(a, b)).withRel("Division"));
		return new ResponseEntity<RepresentationModel>(model, HttpStatus.OK);
	}
	
	@GetMapping("/sum/{a}/{b}")
	public HttpEntity<MathResult> sum(@PathVariable int a, @PathVariable int b) {
		MathResult mathResult = new MathResult();
		mathResult.setOperation("Sum");
		mathResult.setResult(a + b);
		mathResult.add(linkTo(methodOn(this.getClass()).subtract(a, b)).withRel("Subtraction"));
		mathResult.add(linkTo(methodOn(this.getClass()).multiply(a, b)).withRel("Multiplication"));
		mathResult.add(linkTo(methodOn(this.getClass()).divide(a, b)).withRel("Division"));
		return new ResponseEntity<>(mathResult, HttpStatus.OK);
	}
	
	@GetMapping("/subtract/{a}/{b}")
	public HttpEntity<MathResult> subtract(@PathVariable int a, @PathVariable int b) {
		MathResult mathResult = new MathResult();
		mathResult.setOperation("Difference");
		mathResult.setResult(a - b);
		mathResult.add(linkTo(methodOn(this.getClass()).sum(a, b)).withRel("Addition"));
		mathResult.add(linkTo(methodOn(this.getClass()).multiply(a, b)).withRel("Multiplication"));
		mathResult.add(linkTo(methodOn(this.getClass()).divide(a, b)).withRel("Division"));
		return new ResponseEntity<>(mathResult, HttpStatus.OK);
	}
	
	@GetMapping("/product/{a}/{b}")
	public HttpEntity<Integer> multiply(@PathVariable int a, @PathVariable int b) {
		return new ResponseEntity<>(a * b, HttpStatus.OK);
	}
	
	@GetMapping("/divide/{a}/{b}")
	public HttpEntity<Integer> divide(@PathVariable int a, @PathVariable int b) {
		return new ResponseEntity<>(a / b, HttpStatus.OK);
	}
}
