# Day 01

* Services: Independently running, self sufficient applications
* API: RESTful services are commonly referred to as API in the industry
* Different ways to make services talk to each other
* __File system__: No locking, synchronization issues; Old style
* __Databases__: Isolation levels; locking taken care; But not proactive; the application has to keep looking for changes
* __Synchronous communication__: Directly talk to each other; Send a request and wait for response; Security; Slowness; Unavailability; In Spring boot you can use __RestTemplate__ for this
* __Fire and Forget__: Similar to synchronous style but you don't wait for response; 
* __Asynchronous communication__: Use middleware or message brokers; Create topics/queues on the broker; Applications publish and subscribe to the topics/queues; RabbitMQ, ActiveMQ, Kafka. Use the template classes in Spring; JmsTemplate, RabbitTemplate, KafkaTemplate


### Some keywords to remember in Messaging world

* __Retention period__: Number of days you want to persist the message in the broker
* __Offset__: The message index
* __Acknowledgement mode__: Manual or Auto
* __Concurrency limit__: Specify how many messages you want to process concurrently

# Day 02

* __HATEOAS__: RESTful APIs emit data plus link to other resources/operations as well; __RepresentationModel__
* __Swagger__: Used to document your APIs. Automatically generates documentation of your controllers and mapping methods
* Monolith
* Distributed services
* Macro services or service based architecture
* __Microservices__: 'micro' is just a label and does not describe anything about the size/granularity of the service
* __Rationale__: GTM, Resilience, Scalability, Agility
* Devops strategy, Business demands the architecture
* UI -> API Gateway -> Microsservices -> [Shared DB] or [DB per service]
* Ecosystem needs to have: Config server, Monitoring dashboard, Gateway, Centralized Logging, Circuitbreaker, Load balancing

# Spring Security

* Add __spring-security__ dependency
* Create a configuration class that extends __WebSecurityConfigurerAdapter__ and override __configure__ method
* Specify which urls need to authenticated

<br/>
* __Step 1__: User name/password is generated
* __Step 2__: Configure default username and password in yml file
* __Step 3__: Use inMemoryAuthentication
* __Step 4__: Use DaoAuthentication or LdapAuthentication; Configure UserDetailsService, datasource etc.
* __Step 5__: OAuth authentication
























