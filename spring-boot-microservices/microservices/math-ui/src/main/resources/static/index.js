const getCube = () => {
	let url = "/cube/" + document.getElementById("num").value;
	fetch(url, { 
		method: "post"
	})
	.then(response => response.text())
	.then(response => {
		document.getElementById("result").innerHTML = response;
	});
};

const getSquare = () => {
	let url = "/square/" + document.getElementById("num").value;
	fetch(url, { 
		method: "post"
	})
	.then(response => response.text())
	.then(response => {
		document.getElementById("result").innerHTML = response;
	});
};
