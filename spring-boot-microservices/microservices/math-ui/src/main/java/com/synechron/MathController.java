package com.synechron;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class MathController {

	@Autowired
	private MathService mathService;
	
	@PostMapping("/cube/{num}")
	public String computeCube(@PathVariable long num) {
		long result = mathService.getCubeOf(num);
		return "Cube of " + num + ": " + result;
	}
	
	@PostMapping("/square/{num}")
	public String computeSquare(@PathVariable long num) {
		long result = mathService.getSquareOf(num);
		return "Square of " + num + ": " + result;		
	}
}
