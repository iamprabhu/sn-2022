package com.synechron;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
public class MathService {
	
	@Autowired
	private RestTemplate restTemplate;
	
	@Value("${cube.service.url}")
	private String cubeServiceUrl;
	
	@Value("${square.service.url}")
	private String squareServiceUrl;

	public long getCubeOf(long num) {
		long result = restTemplate.getForObject(cubeServiceUrl + "/" + num, Long.class);
		return result;
	}

	public long getSquareOf(long num) {
		long result = restTemplate.getForObject(squareServiceUrl + "/" + num, Long.class);
		return result;
	}

}
