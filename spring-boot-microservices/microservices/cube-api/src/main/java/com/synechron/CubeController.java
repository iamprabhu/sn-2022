package com.synechron;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

@RestController
public class CubeController {

	@Autowired
	private MathPublisherService mathPublisherService;
	
	@GetMapping("/cube/{num}")
	public HttpEntity<Long> computeCube(@PathVariable long num) {
		long result = num * num * num;
		String message = "Cube of " + num + " is " + result;
		mathPublisherService.publish(message);
		return new ResponseEntity<>(result, HttpStatus.OK);
	}
}
