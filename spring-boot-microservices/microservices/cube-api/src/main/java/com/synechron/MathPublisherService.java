package com.synechron;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

@Service
public class MathPublisherService {

	private Logger logger = LoggerFactory.getLogger(MathPublisherService.class);
	
	@Autowired
	private RestTemplate restTemplate;
	
	@Value("${math.publisher.url}")
	private String mathPublisherUrl;
	
	public void publish(String message) {
		try {
			HttpEntity<Void> responseVoid = restTemplate.postForEntity(mathPublisherUrl + "?data=" + message, null, Void.class);
			logger.info("Published {}", message);
		} catch (RestClientException e) {
			logger.error("Error publishing {}. Reason: {}", message, e.getMessage());
		}
	}

}
