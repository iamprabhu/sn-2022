package com.synechron;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class SquareController {

	@Autowired
	private MathPublisherService mathPublisherService;
	
	@GetMapping("/square/{num}")
	public HttpEntity<Long> computeSquare(@PathVariable long num) {
		System.out.println("********computeSquare called on " + Thread.currentThread().getName());
		long result = num * num;
		String message = "Square of " + num + " is " + result;
		mathPublisherService.publish(message);
		return new ResponseEntity<>(result, HttpStatus.OK);
	}
}
