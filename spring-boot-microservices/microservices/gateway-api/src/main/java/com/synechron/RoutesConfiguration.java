package com.synechron;

import org.springframework.cloud.gateway.route.RouteLocator;
import org.springframework.cloud.gateway.route.builder.RouteLocatorBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class RoutesConfiguration {

	// http://localhost:7000/cu/12  (route to) http://localhost:9091/cube/12
	// http://localhost:7000/sq/12	(route to) http://localhost:9092/square/12

	// http://localhost:7000/cube/12  (route to) http://localhost:9091/cube/12
	// http://localhost:7000/math/api/v1/cube/12  (route to) http://localhost:9091/math/1.0/cube/12
	// http://localhost:7000/math/api/v1/cube/12  (route to) http://localhost:9091/math/1.1/cube/12
	// http://localhost:7000/math/api/v2/cube/12  (route to) http://localhost:9091/math/2.0/cube/12



	@Bean
	public RouteLocator configureRoutes(RouteLocatorBuilder builder) {
		return builder
				.routes()
				.route("route-for-math-analytics-service", 
						route -> route
						.path("/analytics/**")
						.uri("lb://math-publisher-api"))

				//please go to eureka, get me the coordinates of cube-api and construct an url by using /cube
				.route("route-for-cube-service", 
						route -> route
						.path("/cu/**")
						.filters(filter -> filter.stripPrefix(1).prefixPath("/cube"))
						.uri("lb://cube-api"))
				//please go to eureka, get me the coordinates of square-api and construct an url by using /square
				.route("route-for-square-service", 
						route -> route
						.path("/sq/**")
						.filters(filter -> filter.stripPrefix(1).prefixPath("/square"))
						.uri("lb://square-api"))//http://localhost:9091/square
				.route("route-for-cube-service-2", 
						route -> route
						.path("/cube/**")
						.uri("lb://cube-api"))
				.route("route-for-cube-service-3", 
						route -> route
						.path("/api/v1/cube/**")
						.filters(filter -> filter.stripPrefix(3).prefixPath("/1.2/cube"))
						.uri("lb://cube-api"))
				.route("route-for-cube-service-for-v2", 
						route -> route
						.path("/api/v2/cube/**")
						.filters(filter -> filter.stripPrefix(3).prefixPath("/2.0/cube"))
						.uri("lb://cube-api"))
				.build();
	}
}
