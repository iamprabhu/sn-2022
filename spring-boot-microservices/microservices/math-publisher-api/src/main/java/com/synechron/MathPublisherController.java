package com.synechron;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class MathPublisherController {

	@Autowired
	private MathPublisherService mathPublisherService;
	
	@PostMapping("/analytics")
	public HttpEntity<Void> publish(@RequestParam String data) {
		mathPublisherService.store(data);
		return new ResponseEntity<>(HttpStatus.OK);
	}
}
