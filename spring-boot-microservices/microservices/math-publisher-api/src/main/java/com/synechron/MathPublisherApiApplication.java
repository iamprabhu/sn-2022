package com.synechron;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableAsync;

@SpringBootApplication
@EnableAsync
public class MathPublisherApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(MathPublisherApiApplication.class, args);
	}

}
