package com.synechron;

import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

@Service
public class MathPublisherService {
	@Autowired
	private RabbitTemplate rabbitTemplate;
	
	@Value("${queue.name}")
	private String queue;

	@Async
	public void store(String data) {
		rabbitTemplate.convertAndSend(queue, data);
	}
}
