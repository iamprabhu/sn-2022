package com.synechron;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.sleuth.annotation.NewSpan;
import org.springframework.stereotype.Service;

@Service
public class MathService {
	
	@Autowired
	private Dummy dummy;
	
	Logger logger = LoggerFactory.getLogger(MathService.class);

	@NewSpan
	public int add(int a, int b) {
		logger.info("MathService add {} {}", a, b);
		dummy.doSomething();
		return a + b;
	}
	
	@NewSpan
	public int subtract(int a, int b) {
		logger.info("MathService subtract {} {}", a, b);
		dummy.doSomething();
		return a - b;
	}
}
