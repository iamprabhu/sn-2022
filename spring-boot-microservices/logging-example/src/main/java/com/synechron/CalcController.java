package com.synechron;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/calc")
public class CalcController {

	Logger logger = LoggerFactory.getLogger(CalcController.class);
	
	@Autowired
	private MathService mathService;
	
	@GetMapping("/sum/{a}/{b}")
	public int add(@PathVariable int a, @PathVariable int b) {
		logger.info("CalcController add {} {}", a, b);
		int result = mathService.add(a, b);
		return result;
	}
	
	@GetMapping("/diff/{a}/{b}")
	public int subtract(@PathVariable int a, @PathVariable int b) {
		logger.info("CalcController subtract {} {}", a, b);
		int result = mathService.subtract(a, b);
		return result;
	}
}
