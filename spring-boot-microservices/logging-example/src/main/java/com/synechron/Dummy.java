package com.synechron;

import org.springframework.cloud.sleuth.annotation.NewSpan;
import org.springframework.stereotype.Component;

@Component
public class Dummy {

	
	@NewSpan
	public void doSomething() {
		
	}
}
