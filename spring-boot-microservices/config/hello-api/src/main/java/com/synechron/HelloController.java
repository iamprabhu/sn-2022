package com.synechron;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RefreshScope
public class HelloController {
	
	@Autowired
	private Dummy dummy;

	@PreDestroy
	public void beforeDestroy() {
		System.out.println("-----HelloController going get destroyed " + this);
		System.out.println(dummy);
	}
	
	@PostConstruct
	public void afterInit() {
		System.out.println("*****HelloController created " + this);
		System.out.println(dummy);
	}
	
	@Value("${welcome}")
	private String welcome;
	
	@GetMapping("/hello")
	public String index() {
		return welcome;
	}

}
