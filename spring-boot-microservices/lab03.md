### Lab03

* Let's modify palindrome exercise in __Lab 01__ a bit to introduce HATEOAS. You can continue working in the same projects you used for **lab01**.

* ui-app talks to the palindrome-api using RestTemplate. It passes the word to an endpoint and gets the result
* Let's modify this behaviour like this

<br/>

* ui-app talks to palindrome-api by sending a request to __/operations/word__.
* __/operations/word__ returns the actual links to find palindrome
* ui-app uses that and sends a request to find if the word is palindrome or not.
* You can optimize the behaviour by not sending the request to __/operations/word__ everytime. 

```
{
	"_links": {
        "Palindrome": {
            "href": "http://localhost:8080/palindrome/word/madam"
        }
	}
}		
```

``` java
  ObjectMapper mapper = new ObjectMapper();
  JsonNode jsonNode = mapper.parse(jsonResponse, JsonNode.class);
  String href = jsonNode.get("_links").get("Palindrome").get("href").toString();

```