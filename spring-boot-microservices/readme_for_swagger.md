## Generate API docs using Swagger

* Create a __MathController__ with the following endpoints

``` java
@RestController
@RequestMapping("/math")
public class MathController {

	@GetMapping("/square/{num}")
	public HttpEntity<Long> computeSquare(@PathVariable long num) {
		return new ResponseEntity<>(num * num, HttpStatus.OK);
	}
	
	@GetMapping("/cube/{num}")
	public HttpEntity<Long> computeCube(@PathVariable long num) {
		return new ResponseEntity<>(num * num * num, HttpStatus.OK);
	}
}
```

* Add the following dependencies to pom.xml

``` xml
		<dependency>
			<groupId>io.springfox</groupId>
			<artifactId>springfox-swagger2</artifactId>
			<version>2.9.2</version>
		</dependency>
		<dependency>
			<groupId>io.springfox</groupId>
			<artifactId>springfox-swagger-ui</artifactId>
			<version>2.9.2</version>
		</dependency>
```

* Create a class __AppConfiguration__ with the following code

``` java

@Configuration
@EnableSwagger2
public class AppConfiguration {

	@Bean
	public Docket swaggerApi() {
		return new Docket(DocumentationType.SWAGGER_2)
				.select()
				.apis(RequestHandlerSelectors.basePackage("<BASE PACKAGE>"))
				.paths(PathSelectors.any())
				.build()
				.apiInfo(new ApiInfoBuilder().version("1.0").title("<TITLE>").description("API documentation v1.0").build());
	}
}

``` 

* Change the __BASE PACKAGE__ and __TITLE__ values accordingly

* Run the app and go to __http://localhost:<port>/swagger-ui.html__

* Use the annotations __@ApiOperation, @ApiResponses, @ApiResponse, @ApiParam__ on your __MathController__ class as given below and observe the changes in the generated documentation 


``` java
@RestController
@RequestMapping("/math")
@Api("Math operations")
public class MathController {

	@ApiOperation(value = "Square of a number")
	@ApiResponse(code = 200, response = Long.class, message = "Square is ")
	@GetMapping("/square/{num}")
	public HttpEntity<Long> computeSquare(@PathVariable @ApiParam(example = "100") long num) {
		return new ResponseEntity<>(num * num, HttpStatus.OK);
	}
	
	@ApiOperation(value = "Cube of a number")
	@GetMapping("/cube/{num}")
	public HttpEntity<Long> computeCube(@PathVariable @ApiParam(example = "100") long num) {
		return new ResponseEntity<>(num * num * num, HttpStatus.OK);
	}
	
}
```