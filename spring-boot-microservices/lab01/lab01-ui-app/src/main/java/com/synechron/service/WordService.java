package com.synechron.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

@Service
public class WordService {
	
	@Autowired
	private RestTemplate restTemplate;
	
	@Autowired
	private ObjectMapper objectMapper;
	
	@Value("${palindrome.service.base.url}")
	private String palindromeBaseUrl;
	
	@Value("${lab03.palindrome.service.operations.url}")
	private String palindromeOperationsUrl;
	
	@Value("${lab03.palindrome.service.operations.key}")
	private String palindromeOperationsKey;
	
	public boolean isPalindrome(String word) {
		//String url = palindromeBaseUrl + "/" + word;
		String url = getPalindromeServiceUrl(word);
		boolean response = restTemplate.getForObject(url, Boolean.class);
		return response;
	}
	
	//Not optimized; You can cache the urls
	//Not handled parsing conditions
	private String getPalindromeServiceUrl(String word) {
		String url = palindromeOperationsUrl + "/" + word;
		String response = restTemplate.getForObject(url, String.class);
		String serviceUrl = null;
		try {
			JsonNode jsonNode = objectMapper.readValue(response, JsonNode.class);
			serviceUrl = jsonNode.get("_links").get(palindromeOperationsKey).get("href").toString().trim();
			serviceUrl = serviceUrl.substring(1, serviceUrl.length() - 1);
		} catch (JsonProcessingException e) {
			e.printStackTrace();
		}
		return serviceUrl;
	}

}
