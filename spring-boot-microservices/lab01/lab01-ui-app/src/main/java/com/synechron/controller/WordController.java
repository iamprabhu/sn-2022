package com.synechron.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.synechron.service.WordService;

@RestController
public class WordController {

	@Autowired
	private WordService wordService;
	
	@PostMapping("/wordplay")
	public HttpEntity<String> checkPalindrome(@RequestParam String word) {
		boolean result = wordService.isPalindrome(word);
		String message = word + " is not a palindrome";
		if(result) {
			message = word + " is a palindrome";
		}
		return new ResponseEntity<>(message, HttpStatus.OK);
	}
}
