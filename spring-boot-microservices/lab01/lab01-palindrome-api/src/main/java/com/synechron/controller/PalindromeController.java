package com.synechron.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.RepresentationModel;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.*;

import com.synechron.service.PalindromeService;

@RestController
public class PalindromeController {
	
	@Autowired
	private PalindromeService palindromeService;

	@GetMapping("/palindrome/{word}")
	public HttpEntity<Boolean> isPalindrome(@PathVariable String word) {
		boolean result = palindromeService.isPalindrome(word);
		return new ResponseEntity<>(result, HttpStatus.OK);
	}
	
	@GetMapping("/palindrome/operations/{word}")
	public HttpEntity<RepresentationModel> getPalindromeOperations(@PathVariable String word) {
		RepresentationModel model = new RepresentationModel();
		model.add(linkTo(methodOn(this.getClass()).isPalindrome(word)).withRel("Palindrome"));
		return new ResponseEntity<>(model, HttpStatus.OK);
	}
}
