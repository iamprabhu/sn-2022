package com.synechron;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Lab01PalindromeApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(Lab01PalindromeApiApplication.class, args);
	}

}
